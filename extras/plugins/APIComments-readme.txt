;=====================================================================================
;
; APIComments-readme.txt
;
;-------------------------------------------------------------------------------------

About
-----

APIComments plugin for x64dbg (32bit plugin), created with the x64dbg plugin for masm
by fearless 2015 - www.LetTheLight.in

The x64dbg plugin SDK for JWasm64 can be downloaded from here:
https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-JWasm64/overview

The x64dbg plugin SDK for Masm can be downloaded from here:
https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-masm/overview


Overview
--------

A plugin to populate the comments with windows api calls


Features
--------

- Add windows api function definition information to the comments



Notes
-----

