;=====================================================================================
;
; APIInfo-readme.txt
;
; v1.0.0.3 - Last updated: 26/06/2016 
;
;-------------------------------------------------------------------------------------

About
-----

APIInfo plugin for x64dbg (32bit plugin), created with the x64dbg plugin for masm
by fearless 2016 - www.LetTheLight.in 

The x64dbg plugin SDK for JWasm64 can be downloaded from here:
https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-JWasm64/overview

The x64dbg plugin SDK for Masm can be downloaded from here:
https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-masm/overview


Overview
--------

A plugin to populate the comments with windows api calls


Features
--------

- Add windows api function definition information to the comments


Installation
------------

Copy APIInfo.dp32 to x32\plugins folder of x64dbg
Extract APIInfoApis.zip which contains the api definition files to the same plugins folder


Notes
-----

- 26/06/2016 updated plugin to use ico, added definitions with plugin
- 01/03/2016 Updated x64dbg SDK for masm to version 1.0.0.2 and recompiled plugin.
- Added function APIInfoLoadMenuIcon to load png resource image as raw bytes 
- Added menu icon for plugin (uses _plugin_menuseticon)
- Added menu entry icons for options and gen api (uses _plugin_menuentryseticon)
